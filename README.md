# Deploying a Merge Portal

This is an example operational playbook which uses MergeTB ansible roles (as
git submodules) along with a custom common role to provision users and other
system-level configurations.

If you clone this repo, then completely re-initialize the git status into a
new repo, you can create a private playbook for your own portal deployment
which includes ssh keys, secrets, and other things custom to your own portal
instance.

## examples

See the example files
examplehosts.yml example-playbook.yml examplevars.yml
for an example of a simple deployment play.

